// Copies a table into a JS object that can be used with table_paste.js.
// Navigate to your table/macro tab and click on your desired table to open it, then run this script.
// The content of the table will be saved to localStorage (and output in the console as well for your viewing pleasure)
// It will be cleared from localStorage when you close the window (not the tab)
// It may take a moment to run. I don't know if touching anything during this process will cause a problem, so don't.

function copyActiveTable(){
    var rows = document.querySelector(".ui-dialog .table").childNodes[0].childNodes;
    var output = [];

    for(const row of rows) {
        row.click();

        var itemText = document.querySelector(".ui-dialog .tableitemeditor input.name").value;
        var itemWeight = document.querySelector(".ui-dialog .tableitemeditor input.weight").value;
        output.push({"weight":itemWeight, "text":itemText})

        document.querySelector(".ui-dialog .tableitemeditor").parentElement.parentElement.querySelector(".ui-dialog-buttonset button:last-child").click();
        
    }

    console.log(output);
    localStorage.setItem("r20TableCopy", JSON.stringify(output));
}
copyActiveTable();