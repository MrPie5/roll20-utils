// Pastes a table created from table_copy.js into a new table. If you haven't run that script first, you probably should.
// This should work no matter which tab of Roll20 you're in, but I recommend closing any dialog boxes that are currently open.
// This will rapidly simulate you changing to the macro/table tab, adding a new rollable table, and then adding and filling new rows in that table.
// This process may take a few seconds, during which your browser may appear to freeze.
// I recommend not touching anything during this time because I have reason to suspect it will break things but I'm not sure how.
// P.S. if you ignore the above recommendation and it does break, let me know what it did because I'm curious but not enough to risk it myself.

function wait(time=100) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('resolved');
        }, time);
    });
}

async function pasteTable(){
    console.log("Pasting table. Please don't touch anything.")
    
    //var rows = [{"weight":1, "text":"test"}, {"weight":2, "text":"testydoo"}, {"weight":0.5, "text":"hello"}]
    var rows = JSON.parse(localStorage.getItem("r20TableCopy"));

    document.getElementById("ui-id-6").click();
    document.getElementById("addrollabletable").click();

    await wait(300); //we need to give Roll20 a sec to make the table we just asked for. 300 milliseconds should be more than enough. Should.

    document.querySelector("#existingrollabletables .rollabletable:last-child").click();

    var tableName = document.querySelector(".ui-dialog .rollabletableeditor input.name");
    tableName.select();

    // The name it will give the new table. You can change this here if you want or just do it in Roll20.
    tableName.value = "my-table"

    for (const row of rows) {
        document.querySelector(".ui-dialog .addtableitem").click();

        var itemName = document.querySelector(".ui-dialog .tableitemeditor input.name");
        itemName.select();
        itemName.value=row.text;
        
        var itemWeight = document.querySelector(".ui-dialog .tableitemeditor input.weight")
        itemWeight.select();
        itemWeight.value=row.weight;

        document.querySelector(".ui-dialog .tableitemeditor").parentElement.parentElement.querySelector(".ui-dialog-buttonset button:first-child").click();
    }
    console.log("Done pasting table. It's safe to touch things now.");
}
pasteTable()