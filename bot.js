// Chat bot for Roll20, to be ran in the browser console while connected to the game.
// Any user can run this, it does not require a connected "bot" account, but having one
// keeps things cleaner.
// MutationObserver code taken from https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver

// Select the node that will be observed for mutations
const targetNode = document.querySelector('#textchat .content');

// Options for the observer (which mutations to observe)
const botConfig = { childList: true };

const prefix = '$';

function send(m) {
    // Put text in the chat area and click the send button
    document.querySelector("#textchat-input textarea").value = "/ooc " + m;
    document.querySelector("#textchat-input > button").click();
}

// Callback function to execute when mutations are observed
function readMessage(mutationsList, observer) {

    // Sending a message creates two mutations. I haven't figured out why, but I know we want the first one
    let mutation = mutationsList[0];

    if(mutation.addedNodes.length > 0) {
        // The actual message is the value of the last child of the first added node
        let message = mutation.addedNodes[0].lastChild.nodeValue;

        // if the message is not null and starts with our prefix
        if(message && message.startsWith(prefix)) {

            // pre-slice our command and argument for convenience
            const command = message.slice(prefix.length).toLowerCase();
            let arg = command.slice(command.indexOf(' ') + 1).toLowerCase();

            const conditionAliases = {
                "blind" : "blinded",
                "charm" : "charmed",
                "deaf" : "deafened",
                "death" : "dead",
                "exhausted" : "exhaustion",
                "frighten" : "frightened",
                "grapple" : "grappled",
                "incap" : "incapacitated",
                "paralyze" : "paralyzed",
                "petrify" : "petrified",
                "poison" : "poisoned",
                "restrain" : "restrained",
                "stun" : "stunned",
                "surprise" : "surprised",
            }
        
            const conditions = {
                "blinded":"• A blinded creature can't see and automatically fails any ability check that requires sight.\n• Attack rolls against the creature have advantage, and the creature's attack rolls have disadvantage.",
                "charmed":"• A charmed creature can't attack the charmer or target the charmer with harmful abilities or magical effects.\n• The charmer has advantage on any ability check to interact socially with the creature.",
                "dead":"• You're fucking dead, bruh. Don't they teach you this shit in school?",
                "deafened":"• A deafened creature can't hear and automatically fails any ability check that requires hearing.",
                "exhaustion":"pending",
                "frightened":"• A frightened creature has disadvantage on ability checks and attack rolls while the source of its fear is within line of sight.\n• The creature can't willingly move closer to the source of its fear.",
                "grappled":"• A grappled creature's speed becomes 0, and it can't benefit from any bonus to its speed.\n• The condition ends if the grappler is incapacitated.\n• The condition also ends if an effect removes the grappled creature from the reach of the grappler or grappling effect, such as when a creature is hurled away by the thunderwave spell.",
                "incapacitated":"• An incapacitated creature can't take actions or reactions.",
                "invisible":"• An invisible creature is impossible to see without the aid of magic or a special sense. For the purpose of hiding, the creature is heavily obscured. The creature's location can be detected by any noise it makes or any tracks it leaves.\n• Attack rolls against the creature have disadvantage, and the creature's attack rolls have advantage.",
                "paralyzed":"• A paralyzed creature is incapacitated and can't move or speak.\n• The creature automatically fails Strength and Dexterity saving throws.\n• Attack rolls against the creature have advantage.\n• Any attack that hits the creature is a critical hit if the attacker is within 5 feet of the creature.",
                "petrified":"• A petrified creature is transformed, along with any nonmagical object it is wearing or carrying, into a solid inanimate substance (usually stone). Its weight increases by a factor of ten, and it ceases aging.\n• The creature is incapacitated, can't move or speak, and is unaware of its surroundings.\n• Attack rolls against the creature have advantage.\n• The creature automatically fails Strength and Dexterity saving throws.\n• The creature has resistance to all damage.\n• The creature is immune to poison and disease, although a poison or disease already in its system is suspended, not neutralized.",
                "poisoned":"• A poisoned creature has disadvantage on attack rolls and ability checks.",
                "prone":"• A prone creature's only movement option is to crawl, unless it stands up and thereby ends the condition.\n• The creature has disadvantage on attack rolls.\n• An attack roll against the creature has advantage if the attacker is within 5 feet of the creature. Otherwise, the attack roll has disadvantage.",
                "restrained":"• A restrained creature's speed becomes 0, and it can't benefit from any bonus to its speed.\n• Attack rolls against the creature have advantage, and the creature's attack rolls have disadvantage.\n• The creature has disadvantage on Dexterity saving throws.",
                "surprised":"• Any character or monster that doesn’t notice a threat is surprised at the start of the encounter.\n• If you’re surprised, you can’t move or take an action on your first turn of the combat, and you can’t take a Reaction until that turn ends. A member of a group can be surprised even if the other Members aren’t.",
                "stunned":"• A stunned creature is incapacitated, can't move, and can speak only falteringly.\n• The creature automatically fails Strength and Dexterity saving throws.\n• Attack rolls against the creature have advantage.",
                "unconscious":"• An unconscious creature is incapacitated, can't move or speak, and is unaware of its surroundings.\n• The creature drops whatever it's holding and falls prone.\n• The creature automatically fails Strength and Dexterity saving throws.\n• Attack rolls against the creature have advantage.\n• Any attack that hits the creature is a critical hit if the attacker is within 5 feet of the creature."
            }

            if(command == "ping") {
                send("pong");
            }

            else if(command.startsWith("condition")){
                if(arg in conditionAliases) {
                    arg = conditionAliases[arg];
                }

                condition = conditions[arg];
                if(condition) {
                    send(condition);
                }
                else {
                    send("Did not recognize that condition. Use \`\`" + prefix + "list conditions\`\` to see what I recognize.")
                }
            
            }

            else if (command.startsWith("list")) {
                if(arg == "conditions") {
                    let toSend = "";
                    Object.keys(conditions).forEach(key => {
                        toSend += key + ', ';
                    })
                    send("I recognize the following conditions: " + toSend.slice(0,-2));
                }
            }

            else if(command == "about"){
                send("I am a Roll20 chat bot created by Mr Pie 5. Source code is available at https://gitlab.com/MrPie5/roll20-utils/-/blob/master/bot.js")
            }

            else if (command == "commands") {
                send(
                    "• ping: I will respond if able\n\
                    • condition x: Give the definition for a condition named x\n\
                    • list conditions: List all the conditions I recognize\n\
                    • about: Basic information about me\n\
                    • commands: Display this message\
                    "
                );
            }
        }
    }
}

// Create an observer instance linked to the callback function
const observer = new MutationObserver(readMessage);

// Start observing the target node for configured mutations
function startBot() {
    try {
        observer.observe(targetNode, botConfig);
        send(
            "Bot online and listening for prefix: " + prefix + "\n" +
            "Use \`\`" + prefix + "commands\`\` to see what I recognize"
        );
    }
    catch(error) {
        console.log(error);
        send("Error: failed to initialize observer");
    }
}

function stopBot() {
    observer.disconnect();
    send("Bot disconnected");
}

startBot();